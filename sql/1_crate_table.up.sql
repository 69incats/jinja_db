SET CHARSET UTF8;

create database if not exists shrine_db default character set utf8;

use shrine_db;

SET CHARACTER_SET_CLIENT = utf8;
SET CHARACTER_SET_CONNECTION = utf8;

create table if not exists shrines (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  name varchar(255) not null,
  location varchar(255) not null, 
  description text,
  created_at datetime default current_timestamp not null,
  updated_at datetime default current_timestamp not null,
  deleted_at datetime,
  primary key (id),
  index(name)
);

create table if not exists deities (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  name varchar(255) not null,
  description text,
  created_at datetime default current_timestamp not null,
  updated_at datetime default current_timestamp not null,
  deleted_at datetime,
  primary key (id),
  index(name)
);

create table if not exists deity_families (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  main_deity_id bigint unsigned not null,
  family_deity_id bigint unsigned not null,
  family_relation varchar(255) not null,
  created_at datetime default current_timestamp not null,
  updated_at datetime default current_timestamp not null,
  deleted_at datetime,
  primary key (id),
  index(family_deity_id, main_deity_id)
);

create table if not exists shrine_deity_relations (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  shrine_id bigint unsigned not null,
  deity_id bigint unsigned not null,
  created_at datetime default current_timestamp not null,
  updated_at datetime default current_timestamp not null,
  deleted_at datetime,
  primary key (id),
  index(shrine_id, deity_id)
);

create table if not exists good_fortunes (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  type varchar(255) not null,
  created_at datetime default current_timestamp not null,
  updated_at datetime default current_timestamp not null,
  deleted_at datetime,
  primary key (id),
  index(type)
);

create table if not exists shrine_good_fortunes_relations (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  shrine_id bigint unsigned not null,
  good_fortune_id bigint unsigned not null,
  created_at datetime default current_timestamp not null,
  updated_at datetime default current_timestamp not null,
  deleted_at datetime,
  primary key (id),
  index(shrine_id, good_fortune_id)
);
