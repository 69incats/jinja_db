package main

import (
	// "github.com/gin-gonic/gin"
	"app/db"
	"app/server"
)

func main() {
	db.Init()
	server.Init()
}
