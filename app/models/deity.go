package models

type Deity struct {
	Model
	Name        string `json:"name"`
	Description string `json:"description"`
}

type DeityRes struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
