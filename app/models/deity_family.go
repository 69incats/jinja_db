package models

type DeityFamily struct {
	Model
	MainDeityID    int    `json:"main_deity_id"`
	FamilyDeityID  int    `json:"family_deity_id"`
	FamilyRelation string `json:"family_relation"`
}
