package models

type Shrine struct {
	Model
	Name        string `json:"name"`
	Location    string `json:"location"`
	Description string `json:"description"`
}

// relation
type ShrineDeityRelation struct {
	Model
	ShrineID int `json:"shrine_id"`
	DeityID  int `json:"deity_id"`
}

type ShrineGoodFortuneRelation struct {
	Model
	ShrineID      int `json:"shrine_id"`
	GoodFortuneID int `json:"good_fortune_id"`
}

// response
type ShrineRes struct {
	ID          int        `json:"id"`
	Name        string     `json:"name"`
	Location    string     `json:"location"`
	Description string     `json:"description"`
	Deities     []DeityRes `json:"deities"`
}
