package service

import (
	"github.com/gin-gonic/gin"

	"app/db"
	"app/models"

	"strconv"
)

type ServShrineGoodFortuneRelation struct{}

type ShrineGoodFortuneRelation models.ShrineGoodFortuneRelation

func (ssgfr *ServShrineGoodFortuneRelation) ListAll() ([]ShrineGoodFortuneRelation, error) {
	db := db.GetDB()

	var sgfr []ShrineGoodFortuneRelation

	if err := db.Find(&sgfr).Error; err != nil {
		return nil, err
	}

	return sgfr, nil
}

func (ssgfr *ServShrineGoodFortuneRelation) Add(ctx *gin.Context) (ShrineGoodFortuneRelation, error) {
	db := db.GetDB()

	var sgfr ShrineGoodFortuneRelation
	id, _ := strconv.Atoi(ctx.Param("id"))
	sgfr.ShrineID = id

	if err := db.Create(&sgfr).Error; err != nil {
		return sgfr, err
	}

	return sgfr, nil
}

func (sd *ServShrineGoodFortuneRelation) Get(ctx *gin.Context) (ShrineGoodFortuneRelation, error) {
	db := db.GetDB()

	var sgfr ShrineGoodFortuneRelation
	id := ctx.Param("id")

	if err := db.First(&sgfr, id).Error; err != nil {
		return sgfr, err
	}

	return sgfr, nil
}

func (sd *ServShrineGoodFortuneRelation) GetByShrineID(shrineID int) ([]ShrineGoodFortuneRelation, error) {
	db := db.GetDB()

	var sgfr []ShrineGoodFortuneRelation

	if err := db.Where("shrine_id=?", shrineID).Find(&sgfr).Error; err != nil {
		return nil, err
	}

	return sgfr, nil
}

func (sd *ServShrineGoodFortuneRelation) Update(ctx *gin.Context) (ShrineGoodFortuneRelation, error) {
	db := db.GetDB()

	var sgfr ShrineGoodFortuneRelation
	if err := ctx.BindJSON(&sgfr); err != nil {
		return sgfr, err
	}

	if err := db.First(&sgfr, sgfr.ID).Error; err != nil {
		return sgfr, err
	}
	db.Save(&sgfr)

	return sgfr, nil
}
