package service

import (
	"github.com/gin-gonic/gin"

	"app/db"
	"app/models"
)

type ServGoodFortune struct{}

type GoodFortune models.GoodFortune

func (sgf ServGoodFortune) ListAll() ([]GoodFortune, error) {
	db := db.GetDB()

	var gf []GoodFortune

	if err := db.Find(&gf).Error; err != nil {
		return nil, err
	}

	return gf, nil
}

func (sgf ServGoodFortune) Add(ctx *gin.Context) (GoodFortune, error) {
	db := db.GetDB()

	var gf GoodFortune
	if err := ctx.BindJSON(&gf); err != nil {
		return gf, err
	}

	if err := db.Create(&gf).Error; err != nil {
		return gf, err
	}

	return gf, nil
}

func (sd ServGoodFortune) GetByID(ctx *gin.Context, id int) (GoodFortune, error) {
	db := db.GetDB()

	var d GoodFortune

	if err := db.First(&d, id).Error; err != nil {
		return d, err
	}

	return d, nil
}

func (sd ServGoodFortune) Update(ctx *gin.Context, id string) (GoodFortune, error) {
	db := db.GetDB()

	var d GoodFortune
	if err := db.First(&d, id).Error; err != nil {
		return d, err
	}

	if err := ctx.BindJSON(&d); err != nil {
		return d, err
	}

	db.Save(&d)

	return d, nil
}
