package service

import (
	"github.com/gin-gonic/gin"

	"app/db"
	"app/models"

	"fmt"
)

type ServShrine struct{}

type Shrine models.Shrine

func (ss *ServShrine) ListAll() ([]Shrine, error) {
	db := db.GetDB()

	var sh []Shrine

	if err := db.Find(&sh).Error; err != nil {
		return nil, err
	}

	return sh, nil
}

func (ss *ServShrine) GetByID(ctx *gin.Context, id int) (Shrine, error) {
	db := db.GetDB()

	var sh Shrine

	if err := db.First(&sh, id).Error; err != nil {
		return sh, err
	}

	return sh, nil
}

func (ss *ServShrine) Add(ctx *gin.Context) (Shrine, error) {
	db := db.GetDB()

	var sh Shrine
	if err := ctx.BindJSON(&sh); err != nil {
		return sh, err
	}

	if err := db.Create(&sh).Error; err != nil {
		return sh, err
	}

	return sh, nil
}

func (ss *ServShrine) Update(ctx *gin.Context, id int) (Shrine, error) {
	db := db.GetDB()

	var sh Shrine
	if err := db.First(&sh, id).Error; err != nil {
		return sh, err
	}

	if err := ctx.BindJSON(&sh); err != nil {
		return sh, err
	}

	fmt.Println(sh)
	db.Save(&sh)

	return sh, nil
}
