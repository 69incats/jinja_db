package service

import (
	"github.com/gin-gonic/gin"

	"app/db"
	"app/models"
)

type ServDeity struct{}

type Deity models.Deity

func (sd ServDeity) ListAll() ([]Deity, error) {
	db := db.GetDB()

	var d []Deity

	if err := db.Find(&d).Error; err != nil {
		return nil, err
	}

	return d, nil
}

func (sd ServDeity) ListInDeityIDs(ids []int) ([]Deity, error) {
	db := db.GetDB()

	var d []Deity

	if err := db.Where("id in (?)", ids).Find(&d).Error; err != nil {
		return nil, err
	}

	return d, nil
}

func (sd ServDeity) Add(ctx *gin.Context) (Deity, error) {
	db := db.GetDB()

	var d Deity
	if err := ctx.BindJSON(&d); err != nil {
		return d, err
	}

	if err := db.Create(&d).Error; err != nil {
		return d, err
	}

	return d, nil
}

func (sd ServDeity) GetByID(ctx *gin.Context, id int) (Deity, error) {
	db := db.GetDB()

	var d Deity
	if err := db.First(&d, id).Error; err != nil {
		return d, err
	}

	return d, nil
}

func (sd ServDeity) Update(ctx *gin.Context, id int) (Deity, error) {
	db := db.GetDB()

	var d Deity
	if err := db.First(&d, id).Error; err != nil {
		return d, err
	}

	if err := ctx.BindJSON(&d); err != nil {
		return d, err
	}

	db.Save(&d)

	return d, nil
}
