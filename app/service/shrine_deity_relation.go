package service

import (
	"github.com/gin-gonic/gin"

	"app/db"
	"app/models"

	"strconv"
)

type ServShrineDeityRelation struct{}

type ShrineDeityRelation models.ShrineDeityRelation

func (sd *ServShrineDeityRelation) ListAll() ([]ShrineDeityRelation, error) {
	db := db.GetDB()

	var sdr []ShrineDeityRelation

	if err := db.Find(&sdr).Error; err != nil {
		return nil, err
	}

	return sdr, nil
}

func (sd *ServShrineDeityRelation) Add(ctx *gin.Context) (ShrineDeityRelation, error) {
	db := db.GetDB()

	var sdr ShrineDeityRelation
	if err := ctx.BindJSON(&sdr); err != nil {
		return sdr, err
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	sdr.ShrineID = id
	if err := db.Create(&sdr).Error; err != nil {
		return sdr, err
	}

	return sdr, nil
}

func (sd *ServShrineDeityRelation) Get(ctx *gin.Context) (ShrineDeityRelation, error) {
	db := db.GetDB()

	var sdr ShrineDeityRelation
	if err := ctx.BindJSON(&sdr); err != nil {
		return sdr, err
	}

	if err := db.First(&sdr, sdr.ID).Error; err != nil {
		return sdr, err
	}

	return sdr, nil
}

func (sd *ServShrineDeityRelation) GetByShrineID(shrineID int) ([]ShrineDeityRelation, error) {
	db := db.GetDB()

	var sdr []ShrineDeityRelation

	if err := db.Where("shrine_id=?", shrineID).Find(&sdr).Error; err != nil {
		return nil, err
	}

	return sdr, nil
}

func (sd *ServShrineDeityRelation) Update(ctx *gin.Context) (ShrineDeityRelation, error) {
	db := db.GetDB()

	var sdr ShrineDeityRelation
	if err := ctx.BindJSON(&sdr); err != nil {
		return sdr, err
	}

	if err := db.First(&sdr, sdr.ID).Error; err != nil {
		return sdr, err
	}
	db.Save(&sdr)

	return sdr, nil
}
