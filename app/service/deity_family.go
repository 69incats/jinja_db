package service

import (
	"github.com/gin-gonic/gin"

	"app/db"
	"app/models"
)

type ServDeityFamily struct{}
type DeityFamily models.DeityFamily

func (sdf ServDeityFamily) ListFamilyByDeityID(ctx *gin.Context, dID int) ([]*DeityFamily, error) {
	db := db.GetDB()

	var df []*DeityFamily

	if err := db.Find(&df).Where("main_deity_id = ?", dID).Error; err != nil {
		return df, err
	}

	return df, nil
}

func (sdf ServDeityFamily) AddFamily(ctx *gin.Context) (DeityFamily, error) {
	db := db.GetDB()

	var df DeityFamily
	if err := ctx.BindJSON(&df); err != nil {
		return df, err
	}

	if err := db.Create(&df).Error; err != nil {
		return df, err
	}

	return df, nil
}

func (sdf ServDeityFamily) UpdateFamily(ctx *gin.Context, fid int) (DeityFamily, error) {
	db := db.GetDB()

	var df DeityFamily
	if err := db.First(&df, fid).Error; err != nil {
		return df, err
	}

	if err := ctx.BindJSON(&df); err != nil {
		return df, err
	}
	db.Save(&df)
	return df, nil
}
