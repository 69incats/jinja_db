package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	db  *gorm.DB
	err error
)

func Init() {
	DB := "mysql"
	User := "root"
	PORT := "3306"
	PASS := "mysql"
	DBNAME := "shrine_db"

	dbConfTmp := "%v:%v@tcp(db:%v)/%v?charset=utf8&parseTime=True&loc=Local"
	dbConf := fmt.Sprintf(dbConfTmp, User, PASS, PORT, DBNAME)

	db, err = gorm.Open(DB, dbConf)

	if err != nil {
		panic(err)
	}
	// defer db.Close()
	// autoMigration()
}

func GetDB() *gorm.DB {
	return db
}

// func autoMigration() {
//      db.AutoMigrate(&entity.Shrine{})
// }
