package controller

import (
	"app/service"
	"fmt"
	"github.com/gin-gonic/gin"
)

type ShrineDeityRelation struct{}

func (c ShrineDeityRelation) Add(ctx *gin.Context) {
	var s service.ServShrineDeityRelation

	ret, err := s.Add(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}
