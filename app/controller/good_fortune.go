package controller

import (
	"app/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

type GoodFortune struct{}

func (c GoodFortune) GetAll(ctx *gin.Context) {
	var s service.ServGoodFortune
	ret, err := s.ListAll()

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c GoodFortune) Add(ctx *gin.Context) {
	var s service.ServGoodFortune

	ret, err := s.Add(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c GoodFortune) Get(ctx *gin.Context) {
	var s service.ServGoodFortune

	id, _ := strconv.Atoi(ctx.Param("id"))
	ret, err := s.GetByID(ctx, id)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c GoodFortune) Update(ctx *gin.Context) {
	var s service.ServGoodFortune

	id := ctx.Param("id")
	ret, err := s.Update(ctx, id)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}
