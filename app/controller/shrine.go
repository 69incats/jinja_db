package controller

import (
	"app/models"
	"app/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

type Shrine struct{}

// shrine
func (c Shrine) GetAll(ctx *gin.Context) {
	var s service.ServShrine
	ret, err := s.ListAll()

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Shrine) Add(ctx *gin.Context) {
	var s service.ServShrine

	ret, err := s.Add(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Shrine) Get(ctx *gin.Context) {

	var ss service.ServShrine
	var ssdr service.ServShrineDeityRelation
	var sd service.ServDeity

	id, _ := strconv.Atoi(ctx.Param("id"))
	shrine, err := ss.GetByID(ctx, id)
	relations, _ := ssdr.GetByShrineID(shrine.ID)

	ret := models.ShrineRes{}

	deityIDs := make([]int, len(relations))
	if relations != nil {
		for i, relation := range relations {
			deityIDs[i] = relation.DeityID
		}

		deities, err := sd.ListInDeityIDs(deityIDs)
		if err != nil {
			fmt.Println(err)
		}
		deitiesSlice := make([]models.DeityRes, len(deities))
		for i, deity := range deities {
			d := models.DeityRes{}
			d.ID = deity.ID
			d.Name = deity.Name
			d.Description = deity.Description
			deitiesSlice[i] = d
		}

		ret.Deities = deitiesSlice
	}

	ret.ID = shrine.ID
	ret.Name = shrine.Name
	ret.Location = shrine.Location
	ret.Description = shrine.Description

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Shrine) Update(ctx *gin.Context) {
	var s service.ServShrine

	id, _ := strconv.Atoi(ctx.Param("id"))
	ret, err := s.Update(ctx, id)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Shrine) AddRelationDeity(ctx *gin.Context) {
	var s service.ServShrineDeityRelation

	ret, err := s.Add(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Shrine) AddRelationGoodFortune(ctx *gin.Context) {
	var s service.ServShrineGoodFortuneRelation

	ret, err := s.Add(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}
