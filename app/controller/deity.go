package controller

import (
	"app/service"

	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Deity struct{}

func (c Deity) GetAll(ctx *gin.Context) {
	var s service.ServDeity
	ret, err := s.ListAll()

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Deity) Add(ctx *gin.Context) {
	var s service.ServDeity

	ret, err := s.Add(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Deity) Get(ctx *gin.Context) {
	var s service.ServDeity

	id, _ := strconv.Atoi(ctx.Param("id"))
	ret, err := s.GetByID(ctx, id)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Deity) Update(ctx *gin.Context) {
	var s service.ServDeity

	strID := ctx.Param("id")
	id, _ := strconv.Atoi(strID)
	ret, err := s.Update(ctx, id)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Deity) GetFamilyList(ctx *gin.Context) {
	var sdf service.ServDeityFamily

	strID := ctx.Param("id")
	id, _ := strconv.Atoi(strID)
	ret, err := sdf.ListFamilyByDeityID(ctx, id)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Deity) AddFamily(ctx *gin.Context) {
	var sdf service.ServDeityFamily

	ret, err := sdf.AddFamily(ctx)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(200, ret)
	}
}

func (c Deity) UpdateFamily(ctx *gin.Context) {
	var sdf service.ServDeityFamily

	familyID := ctx.Param("family_id")
	fID, _ := strconv.Atoi(familyID)
	ret, err := sdf.UpdateFamily(ctx, fID)

	if err != nil {
		ctx.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		ctx.JSON(204, ret)
	}
}
