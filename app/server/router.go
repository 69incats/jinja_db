package server

import (
	"app/controller"
	"github.com/gin-gonic/gin"
)

func Init() {
	r := router()
	r.Run(":8081")
}

func router() *gin.Engine {
	r := gin.Default()

	sh := r.Group("/shrine")
	{
		ctrl := controller.Shrine{}
		sh.GET("", ctrl.GetAll)
		sh.POST("", ctrl.Add)
		sh.GET("/:id", ctrl.Get)
		sh.PATCH("/:id", ctrl.Update)

		sh.POST("/:id/relation/deity", ctrl.AddRelationDeity)
		sh.POST("/:id/relation/good_fortune", ctrl.AddRelationGoodFortune)
	}

	deity := r.Group("/deity")
	{
		ctrl := controller.Deity{}
		deity.GET("", ctrl.GetAll)
		deity.POST("", ctrl.Add)
		deity.GET("/:id", ctrl.Get)
		deity.PATCH("/:id", ctrl.Update)

		deity.GET("/:id/family", ctrl.GetFamilyList)
		deity.POST("/:id/family", ctrl.AddFamily)
		deity.POST("/:id/family/:family_id", ctrl.UpdateFamily)
	}

	goodFortune := r.Group("/good_fortune")
	{
		ctrl := controller.GoodFortune{}
		goodFortune.GET("", ctrl.GetAll)
		goodFortune.POST("", ctrl.Add)
		goodFortune.GET("/:id", ctrl.Get)
		goodFortune.PATCH("/:id", ctrl.Update)
	}

	return r
}
